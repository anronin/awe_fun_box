# AweFunBox

To start using the application:

  * PostgreSQL must be installed and running
  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Generate access token on [`GitHub`](https://github.com/settings/tokens/new) and save it
  * Export access token as shell variable `export GITHUB_ACCESS_TOKEN="saved access token"`
  * Populate database with sample data by `mix run priv/repo/seeds.exs`
  * Start Phoenix endpoint with `mix phx.server`
  * Send message `:update` to the `AweFunBox.Updater` process to get all data from [`h4cc/awesome-elixir`](https://github.com/h4cc/awesome-elixir) or wait 24 hours :)

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
