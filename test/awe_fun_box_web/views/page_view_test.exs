defmodule AweFunBoxWeb.PageViewTest do
  use AweFunBoxWeb.ConnCase, async: true
  alias AweFunBoxWeb.PageView

  test "convert_topic_name/1" do
    assert PageView.convert_topic_name("Algorithms and Data structures") == "algorithms-and-data-structures"
  end

  test "days_from_last_commit/1" do
    days_earlier = 4
    date = Date.utc_today() |> Date.add(-days_earlier) |> Date.to_iso8601()
    naivedatetime = NaiveDateTime.from_iso8601!("#{date} 00:00:00")
    assert PageView.days_from_last_commit(naivedatetime) == days_earlier
  end
end
