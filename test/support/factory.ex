defmodule AweFunBox.Factory do
  alias AweFunBox.Repo

  def build(factory_name, attributes) do
    factory_name |> build() |> struct(attributes)
  end

  def insert!(factory_name, attributes \\ []) do
    factory_name |> build(attributes) |> Repo.insert!()
  end

  def build(:collection) do
    %AweFunBox.Collection{
      topic: "topic-#{System.unique_integer([:positive])}",
      description: "Some collection description",
      libraries: []
    }
  end

  def build(:collection_with_libraries) do
    %AweFunBox.Collection{
      topic: "topic-#{System.unique_integer([:positive])}",
      description: "Some collection description",
      libraries:
        for _ <- 1..3 do
          build(:library)
        end
    }
  end

  def build(:library) do
    %AweFunBox.Library{
      name: "name-#{System.unique_integer([:positive])}",
      description: "Some library description",
      url: "url",
      stargazers_count: Enum.random(1..2000),
      last_commit_date: NaiveDateTime.utc_now()
    }
  end
end
