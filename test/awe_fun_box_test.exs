defmodule AweFunBoxTest do
  use AweFunBox.DataCase
  alias AweFunBox.{Collection, Library}
  import Mox

  describe "delegations" do
    test "all_collections_with_libraries/1 presence" do
      assert 1 == AweFunBox.__info__(:functions) |> Keyword.fetch!(:all_collections_with_libraries)
    end
  end

  describe "fetch_awesomeness/2" do
    setup do
      verify_on_exit!()
      set_mox_global()

      adapter = AweFunBox.DummyRequest
      stub(adapter, :request, &dummy_request/1)

      {:ok, adapter: adapter}
    end

    test "inserts parsed collections", ctx do
       data = File.read!("test/fixtures/readme.md")
       assert [{:ok, collection}] = AweFunBox.fetch_awesomeness(data, ctx.adapter)
       assert collection.topic == Repo.one(Collection).topic
       assert Enum.count(collection.libraries) == 3
       assert collection.libraries == Repo.all(Library)
    end
  end

  defp dummy_request(url) do
    stargazers_count_body = Poison.encode!(%{"stargazers_count" => 10})
    last_commit_date_body = Poison.encode!([%{"commit" => %{"author" => %{"date" => NaiveDateTime.utc_now()}}}])

    if String.ends_with?(url, "/commits"), do: {:ok, last_commit_date_body}, else: {:ok, stargazers_count_body}
  end
end
