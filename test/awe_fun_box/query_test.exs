defmodule AweFunBox.QueryTest do
  use AweFunBox.DataCase
  alias AweFunBox.{Collection, Library, Query}
  import AweFunBox.Factory

  describe "insert_collections/1" do
    setup do
      parsed_collections = [
        %{
          topic: "Actors",
          description: "Libraries and tools for working with actors and such.",
          libraries: [
            %{
              description: "Pipelined flow processing engine.",
              last_commit_date: NaiveDateTime.utc_now(),
              name: "dflow",
              stargazers_count: 10,
              url: "https://github.com/dalmatinerdb/dflow"
            },
            %{
              description: "Helpers for easier implementation of actors in Elixir.",
              last_commit_date: NaiveDateTime.utc_now(),
              name: "exactor",
              stargazers_count: 20,
              url: "https://github.com/sasa1977/exactor"
            }
          ]
        },
        %{
          topic: "Algorithms and Data structures",
          description: "Libraries and implementations of algorithms and data structures.",
          libraries: []
        }
      ]

      Query.insert_collections(parsed_collections)

      :ok
    end

    test "inserts only valid collections" do
      assert collection = Collection |> Repo.one() |> Repo.preload(:libraries)
      assert collection.topic == "Actors"
      assert collection.description == "Libraries and tools for working with actors and such."
      assert Enum.count(collection.libraries) == 2
      assert libraries = Repo.all(Library)
      assert_in_libraries(collection, libraries)
    end

    test "update collections" do
      assert collection = Collection |> Repo.one() |> Repo.preload(:libraries)
      assert Enum.count(collection.libraries) == 2
      assert libraries = Repo.all(Library)
      assert_in_libraries(collection, libraries)

      updated_collections = [
        %{
          topic: "Actors",
          description: "Libraries and tools for working with actors and such.",
          libraries: [
            %{
              description: "Indifferent access for Elixir maps/list/tuples with custom key conversion.",
              last_commit_date: NaiveDateTime.utc_now(),
              name: "indifferent",
              stargazers_count: 40,
              url: "https://github.com/vic/indifferent"
            }
          ]
        }
      ]

      Query.insert_collections(updated_collections)
      assert collection = Collection |> Repo.one() |> Repo.preload(:libraries)
      assert Enum.count(collection.libraries) == 1
      assert library = Repo.one(Library)
      assert library.url == "https://github.com/vic/indifferent"
      assert library.name == "indifferent"
      assert(library.stargazers_count == 40)
    end
  end

  describe "all_collections/1" do
    setup do
      library_10_stars = build(:library, stargazers_count: 10)
      library_20_stars = build(:library, stargazers_count: 20)
      collection = insert!(:collection_with_libraries, libraries: [library_10_stars, library_20_stars])

      {:ok, collection: collection, library_20_stars: library_20_stars}
    end

    test "returns all libraries where stargazers_count greater or even min_stars", ctx do
      # min_stars = 10
      assert collections = Query.all_collections(10)
      collection = List.first(collections)
      libraries = Repo.all(Library)

      assert collection == ctx.collection
      assert Enum.count(collection.libraries) == 2
      assert_in_libraries(collection, libraries)

      # min_stars = 11
      assert collections = Query.all_collections(11)
      collection = List.first(collections)
      library_20_stars = Repo.get_by(Library, name: ctx.library_20_stars.name)

      assert collection.topic == ctx.collection.topic
      assert Enum.count(collection.libraries) == 1
      assert_in_libraries(collection, [library_20_stars])
    end
  end

  defp assert_in_libraries(collection, libraries) do
    Enum.each(collection.libraries, fn library -> assert library in libraries end)
  end
end
