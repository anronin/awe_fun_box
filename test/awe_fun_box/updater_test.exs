defmodule AweFunBox.UpdaterTest do
  use AweFunBox.DataCase
  alias AweFunBox.{Collection, Updater}
  import Mox

  setup do
    verify_on_exit!()
    set_mox_global()

    stub(AweFunBox.DummyRequest, :request, &dummy_request/1)

    :ok
  end

  test "fetch awesome elixir data" do
    refute Repo.one(Collection)

    Updater |> Process.whereis() |> send(:update)
    Process.sleep(500)

    assert collection = Repo.one(Collection) |> Repo.preload(:libraries)
    assert collection.topic == "Actors"
    assert collection.description == "Libraries and tools for working with actors and such."
    assert Enum.count(collection.libraries) == 3
  end

  defp dummy_request(url) do
    stargazers_count_body = Poison.encode!(%{"stargazers_count" => 10})
    last_commit_date_body = Poison.encode!([%{"commit" => %{"author" => %{"date" => NaiveDateTime.utc_now()}}}])

    cond do
      String.ends_with?(url, "/commits") -> {:ok, last_commit_date_body}
      String.ends_with?(url, ".md") -> {:ok, File.read!("test/fixtures/readme.md")}
      true -> {:ok, stargazers_count_body}
    end
  end
end
