defmodule AweFunBox.ParserTest do
  use AweFunBox.DataCase
  alias AweFunBox.Parser

  test "parse/1" do
    assert [data] = File.read!("test/fixtures/readme.md") |> Parser.parse()

    assert data == %{
             description: "Libraries and tools for working with actors and such.",
             libraries: [
               %{
                 description: "Pipelined flow processing engine.",
                 last_commit_date: nil,
                 name: "dflow",
                 stargazers_count: nil,
                 url: "https://github.com/dalmatinerdb/dflow"
               },
               %{
                 description: "Helpers for easier implementation of actors in Elixir.",
                 last_commit_date: nil,
                 name: "exactor",
                 stargazers_count: nil,
                 url: "https://github.com/sasa1977/exactor"
               },
               %{
                 description: "A Port Wrapper which forwards cast and call to a linked Port.",
                 last_commit_date: nil,
                 name: "exos",
                 stargazers_count: nil,
                 url: "https://github.com/awetzel/exos"
               }
             ],
             topic: "Actors"
           }
  end
end
