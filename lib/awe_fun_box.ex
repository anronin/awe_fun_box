defmodule AweFunBox do
  @moduledoc false

  alias AweFunBox.{Parser, Query}

  defdelegate all_collections_with_libraries(min_stars), to: Query, as: :all_collections

  @callback request(String.t()) :: {:ok, String.t()} | {:error, atom | String.t()}

  @spec fetch_awesomeness(String.t(), module()) :: list
  def fetch_awesomeness(data, adapter) do
    data
    |> Parser.parse()
    |> fetch_repo_info(adapter)
    |> Query.insert_collections()
  end

  defp fetch_repo_info(collections, adapter) do
    for collection <- collections do
      libraries =
        collection.libraries
        |> Task.async_stream(&update_library_info(&1, adapter))
        |> Enum.reduce([], fn
          {:ok, library}, acc when is_map(library) -> [library | acc]
          {:ok, _}, acc -> acc
          {:error, _}, acc -> acc
        end)

      %{collection | libraries: libraries}
    end
  end

  defp update_library_info(library, adapter) do
    repos_url = String.replace_leading(library.url, "https://github.com", "https://api.github.com/repos")

    with {:ok, stargazers_count} <- stargazers_count(repos_url, adapter),
         {:ok, last_commit_date} <- last_commit_date(repos_url, adapter),
         do: %{library | stargazers_count: stargazers_count, last_commit_date: last_commit_date}
  end

  defp stargazers_count(url, adapter) do
    with {:ok, body} <- adapter.request(url) do
      if stargazers_count = Poison.decode!(body)["stargazers_count"], do: {:ok, stargazers_count}
    end
  end

  defp last_commit_date(url, adapter) do
    with {:ok, body} <- adapter.request(url <> "/commits") do
      recent_commit_res = body |> Poison.decode!() |> List.first()
      if last_commit_date = recent_commit_res["commit"]["author"]["date"], do: {:ok, last_commit_date}
    end
  end
end
