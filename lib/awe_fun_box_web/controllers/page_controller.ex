defmodule AweFunBoxWeb.PageController do
  use AweFunBoxWeb, :controller

  def index(conn, params) do
    collections = AweFunBox.all_collections_with_libraries(params["min_stars"])
    render(conn, "index.html", collections: collections)
  end
end
