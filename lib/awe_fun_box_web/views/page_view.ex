defmodule AweFunBoxWeb.PageView do
  use AweFunBoxWeb, :view

  @spec convert_topic_name(String.t()) :: String.t()
  def convert_topic_name(topic) do
    topic |> String.downcase |> String.replace(~r/\W/, "-")
  end

  @spec days_from_last_commit(NaiveDateTime.t()) :: integer()
  def days_from_last_commit(datetime) do
    Date.diff(Date.utc_today(), datetime)
  end
end
