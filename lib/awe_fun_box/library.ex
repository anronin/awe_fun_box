defmodule AweFunBox.Library do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  schema "libraries" do
    belongs_to :collection, AweFunBox.Collection

    field :name, :string
    field :description, :string
    field :url, :string
    field :stargazers_count, :integer
    field :last_commit_date, :naive_datetime

    timestamps(type: :utc_datetime)
  end

  @required_fields [:name, :description, :url, :stargazers_count, :last_commit_date]

  @spec changeset(map, map) :: Ecto.Changeset.t()
  def changeset(libraries, params \\ %{}) do
    libraries
    |> cast(params, @required_fields)
    |> validate_required(@required_fields)
    |> validate_lengths([:name, :description, :url])
    |> validate_number(:stargazers_count, greater_than_or_equal_to: 0)
  end

  defp validate_lengths(changeset, fields) do
    Enum.reduce(fields, changeset, &validate_length(&2, &1, max: 255))
  end
end
