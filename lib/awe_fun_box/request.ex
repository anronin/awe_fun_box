defmodule AweFunBox.Request do
  @moduledoc false

  @behaviour AweFunBox

  @doc """
  ### Examples:

    iex> {:ok, body} = AweFunBox.Request.request("example.com")
    iex> body =~ "<title>Example Domain</title>"
    true

    iex> AweFunBox.Request.request("test.com")
    {:error, :invalid_request}

    iex> AweFunBox.Request.request("test")
    {:error, :nxdomain}

  """
  @impl true
  def request(url) do
    access_token = Application.get_env(:awe_fun_box, :access_token, "")

    case HTTPoison.get(url <> "?access_token=#{access_token}") do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        {:ok, body}

      {:ok, %HTTPoison.Response{status_code: status}} when status != 200 ->
        {:error, :invalid_request}

      {:error, %HTTPoison.Error{reason: reason}} ->
        {:error, reason}
    end
  end
end
