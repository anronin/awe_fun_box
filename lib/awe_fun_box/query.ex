defmodule AweFunBox.Query do
  @moduledoc false

  alias AweFunBox.{Collection, Library, Repo}
  import Ecto.Query

  @spec insert_collections(map) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def insert_collections(collections) do
    for collection <- collections do
      exist_collection = Collection |> Repo.get_by(topic: collection.topic) |> Repo.preload(:libraries)

      (exist_collection || %Collection{})
      |> Collection.changeset(collection)
      |> Repo.insert_or_update()
    end
  end

  @spec all_collections(String.t()) :: [] | [%Collection{}]
  def all_collections(min_stars) do
    min_stars = if min_stars, do: min_stars, else: 0
    library_query = from(l in Library, where: l.stargazers_count >= ^min_stars, order_by: :name)
    from(c in Collection, preload: [libraries: ^library_query], order_by: :topic) |> Repo.all()
  end
end
