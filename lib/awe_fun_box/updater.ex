defmodule AweFunBox.Updater do
  @moduledoc false

  use GenServer

  def start_link(args \\ []) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  @impl true
  def init(args) do
    opts = Keyword.merge(initial_state(), args)

    :timer.send_interval(opts[:interval], :update)

    {:ok, opts}
  end

  @impl true
  def handle_info(:update, state) do
    adapter = state[:adapter]

    with {:ok, body} <- adapter.request(state[:awesome_url]),
         fetched_collections <- AweFunBox.fetch_awesomeness(body, adapter) do
      {:noreply, save_statistics(fetched_collections, state)}
    else
      _ -> {:noreply, state}
    end
  end

  @impl true
  def handle_info(_msg, state) do
    {:noreply, state}
  end

  defp save_statistics(collections, state) do
    updated_collections =
      collections
      |> Enum.reduce(%{succeed: [], failed: []}, fn
        {:ok, inserted_collection}, acc -> %{acc | succeed: [inserted_collection | acc.succeed]}
        {:error, changeset}, acc -> %{acc | failed: [changeset | acc.failed]}
      end)
      |> Map.put(:updated_at, NaiveDateTime.utc_now())

    Keyword.put(state, :collections, updated_collections)
  end

  defp initial_state() do
    [
      interval: Application.get_env(:awe_fun_box, :interval, :timer.hours(24)),
      adapter: Application.get_env(:awe_fun_box, :adapter, AweFunBox.Request),
      awesome_url: Application.get_env(:awe_fun_box, :awesome_url)
    ]
  end
end
