defmodule AweFunBox.Collection do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  schema "collections" do
    has_many :libraries, AweFunBox.Library, on_delete: :delete_all, on_replace: :delete

    field :topic, :string
    field :description, :string

    timestamps(type: :utc_datetime)
  end

  @required_fields [:topic, :description]

  @spec changeset(map, map) :: Ecto.Changeset.t()
  def changeset(collection, params \\ %{}) do
    collection
    |> cast(params, @required_fields)
    |> validate_required(@required_fields)
    |> validate_lengths(@required_fields)
    |> cast_assoc(:libraries, required: true, with: &AweFunBox.Library.changeset/2)
  end

  defp validate_lengths(changeset, fields) do
    Enum.reduce(fields, changeset, &validate_length(&2, &1, max: 255))
  end
end
