defmodule AweFunBox.Parser do
  @moduledoc false

  @spec parse(String.t()) :: list(map)
  def parse(data) do
    data
    |> String.split("# Resources")
    |> List.first()
    |> String.split("## ")
    |> Enum.reject(&invalid_string/1)
    |> Enum.reduce([], &[collect(&1) | &2])
  end

  defp collect(data) do
    [topic_desc, raw_libraries] = String.split(data, "*\n", parts: 2)
    [topic, desc] = String.split(topic_desc, "\n", parts: 2)

    libraries =
      raw_libraries
      |> String.trim()
      |> String.split("\n")
      |> Enum.map(&libraries_info/1)
      |> Enum.reject(&is_nil/1)

    %{topic: topic, description: String.trim_leading(desc, "*"), libraries: libraries}
  end

  defp libraries_info(data) do
    [name_url, desc] = String.split(data, " - ", parts: 2)
    name = Regex.run(~r/(?<=\[).+?(?=\])/, name_url) |> List.first()
    url = Regex.run(~r/(?<=\().+?(?=\))/, name_url) |> List.first()

    if String.starts_with?(url, "https://github.com") do
      %{name: name, url: url, description: desc, stargazers_count: nil, last_commit_date: nil}
    end
  end

  defp invalid_string(string) do
    String.starts_with?(string, "# Awesome Elixir [![Build Status]") or String.trim(string) == ""
  end
end
