# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :awe_fun_box,
  ecto_repos: [AweFunBox.Repo],
  awesome_url: "https://raw.githubusercontent.com/h4cc/awesome-elixir/master/README.md",
  access_token: System.get_env("GITHUB_ACCESS_TOKEN")

# Configures the endpoint
config :awe_fun_box, AweFunBoxWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "HMOdq3L0aVeid/xgYZv7zPcQ+XrPNGCscrawxPHBrsmmbIxjDAwMDfmCdipZtx+e",
  render_errors: [view: AweFunBoxWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: AweFunBox.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :hackney, max_connections: 10000

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
