use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :awe_fun_box, AweFunBoxWeb.Endpoint,
  http: [port: 4001],
  server: false

config :awe_fun_box, adapter: AweFunBox.DummyRequest

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :awe_fun_box, AweFunBox.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "awe_fun_box_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
