defmodule AweFunBox.Repo.Migrations.CreateCollectionsTable do
  use Ecto.Migration

  def change() do
    create table(:collections) do
      add :topic, :string, null: false, size: 255
      add :description, :string, null: false, size: 255

      timestamps()
    end
  end
end
