defmodule AweFunBox.Repo.Migrations.CreateLibrariesTable do
  use Ecto.Migration

  def change() do
    create table(:libraries) do
      add :name, :string, null: false, size: 255
      add :description, :string, null: false, size: 255
      add :url, :string, null: false, size: 255
      add :stargazers_count, :integer, null: false
      add :last_commit_date, :utc_datetime, null: false

      add :collection_id, references(:collections, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
