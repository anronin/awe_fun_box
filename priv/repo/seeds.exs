collections = [
  %AweFunBox.Collection{
    description: "Web development frameworks.",
    libraries: [
      %AweFunBox.Library{
        description: "An Elixir web micro-framework.",
        last_commit_date: ~N[2018-07-25 17:53:36],
        name: "trot",
        stargazers_count: 311,
        url: "https://github.com/hexedpackets/trot"
      },
      %AweFunBox.Library{
        description: "Modular web framework for Elixir.",
        last_commit_date: ~N[2017-02-07 03:56:12],
        name: "sugar",
        stargazers_count: 362,
        url: "https://github.com/sugar-framework/sugar"
      },
      %AweFunBox.Library{
        description: "Micro-REST framework with typed JSON.",
        last_commit_date: ~N[2018-04-02 12:58:28],
        name: "rest",
        stargazers_count: 56,
        url: "https://github.com/synrc/rest"
      },
      %AweFunBox.Library{
        description: "Simple Elixir implementation of a [jsonapi.org](http://jsonapi.org) server.",
        last_commit_date: ~N[2016-02-22 14:44:56],
        name: "relax",
        stargazers_count: 126,
        url: "https://github.com/AgilionApps/relax"
      }
    ],
    topic: "Frameworks"
  },
  %AweFunBox.Collection{
    description: "Standalone component from web development frameworks.",
    libraries: [
      %AweFunBox.Library{
        description: "An XML-RPC parser/formatter for Elixir, with full support for datatype mapping.",
        last_commit_date: ~N[2015-08-17 03:54:47],
        name: "weebo",
        stargazers_count: 3,
        url: "https://github.com/stevenschobert/weebo"
      },
      %AweFunBox.Library{
        description: "An Elixir plug to support legacy APIs that use a rails-like trailing format.",
        last_commit_date: ~N[2017-03-27 11:55:11],
        name: "trailing_format_plug",
        stargazers_count: 20,
        url: "https://github.com/mschae/trailing_format_plug"
      }
    ],
    topic: "Framework Components"
  },
  %AweFunBox.Collection{
    description: "Libraries and implementations for working with files and directories.",
    libraries: [
      %AweFunBox.Library{
        description: "Filename sanitization for Elixir.",
        last_commit_date: ~N[2018-01-18 22:54:39],
        name: "zarex",
        stargazers_count: 14,
        url: "https://github.com/ricn/zarex"
      },
      %AweFunBox.Library{
        description: "An Elixir library to make file sizes human-readable.",
        last_commit_date: ~N[2017-05-15 14:54:36],
        name: "sizeable",
        stargazers_count: 16,
        url: "https://github.com/arvidkahl/sizeable"
      },
      %AweFunBox.Library{
        description: "Flexible file upload and attachment library for Elixir.",
        last_commit_date: ~N[2018-06-19 20:05:25],
        name: "arc",
        stargazers_count: 798,
        url: "https://github.com/stavro/arc"
      }
    ],
    topic: "Files and Directories"
  },
  %AweFunBox.Collection{
    description: "Libraries working with feeds like RSS or ATOM.",
    libraries: [
      %AweFunBox.Library{
        description: "RSS/Atom parser built on erlang's xmerl xml parser.",
        last_commit_date: ~N[2017-01-26 22:01:42],
        name: "feedme",
        stargazers_count: 7,
        url: "https://github.com/umurgdk/elixir-feedme"
      },
      %AweFunBox.Library{
        description: "RSS feed parser. Simple wrapper for feeder.",
        last_commit_date: ~N[2017-05-09 06:29:53],
        name: "feeder_ex",
        stargazers_count: 53,
        url: "https://github.com/manukall/feeder_ex"
      },
      %AweFunBox.Library{
        description: "Parse RSS and Atom feeds.",
        last_commit_date: ~N[2018-03-20 07:07:44],
        name: "feeder",
        stargazers_count: 29,
        url: "https://github.com/michaelnisi/feeder"
      },
      %AweFunBox.Library{
        description: "ATOM feed builder with a focus on standards compliance, security and extensibility.",
        last_commit_date: ~N[2018-07-27 16:05:17],
        name: "atomex",
        stargazers_count: 7,
        url: "https://github.com/Betree/atomex"
      }
    ],
    topic: "Feeds"
  },
  %AweFunBox.Collection{
    description:
      "Libraries to manage feature toggles (AKA feature flags): ON/OFF values that can be toggled at runtime through some interface",
    libraries: [
      %AweFunBox.Library{
        description: "A feature toggle library using redis or SQL (using Ecto) as a backing service.",
        last_commit_date: ~N[2017-04-27 00:46:28],
        name: "molasses",
        stargazers_count: 63,
        url: "https://github.com/securingsincity/molasses"
      },
      %AweFunBox.Library{
        description:
          "A feature toggle library using Redis or Ecto for persistance, an ETS cache for speed and PubSub for distributed cache busting. Comes with a management web UI for Phoenix and Plug.",
        last_commit_date: ~N[2018-07-28 22:56:12],
        name: "fun_with_flags",
        stargazers_count: 163,
        url: "https://github.com/tompave/fun_with_flags"
      },
      %AweFunBox.Library{
        description: "Feature flipping for the Elixir world.",
        last_commit_date: ~N[2018-03-20 14:47:48],
        name: "flippant",
        stargazers_count: 27,
        url: "https://github.com/sorentwo/flippant"
      }
    ],
    topic: "Feature Flags and Toggles"
  },
  %AweFunBox.Collection{
    description: "Example code and stuff too funny or curious not to mention.",
    libraries: [
      %AweFunBox.Library{
        description: "A command line weather app built using Elixir.",
        last_commit_date: ~N[2017-07-28 03:23:08],
        name: "weather",
        stargazers_count: 62,
        url: "https://github.com/tacticiankerala/elixir-weather"
      },
      %AweFunBox.Library{
        description: "A very simple (and barely-functioning) Ruby runner for Elixir.",
        last_commit_date: ~N[2015-01-27 23:08:50],
        name: "rubix",
        stargazers_count: 2,
        url: "https://github.com/YellowApple/Rubix"
      },
      %AweFunBox.Library{
        description:
          "Elixir / Phoenix implementation of [RealWorld.io](https://realworld.io/) backend specs - a Medium clone.",
        last_commit_date: ~N[2018-07-05 16:30:19],
        name: "real world example app",
        stargazers_count: 291,
        url: "https://github.com/gothinkster/elixir-phoenix-realworld-example-app"
      },
      %AweFunBox.Library{
        description: "A shooting fault-tolerant doors for distributed portal data-transfer application in Elixir.",
        last_commit_date: ~N[2017-02-28 09:06:08],
        name: "portal",
        stargazers_count: 34,
        url: "https://github.com/josevalim/portal"
      },
      %AweFunBox.Library{
        description: "Flipping tables with butler.",
        last_commit_date: ~N[2016-02-03 20:34:42],
        name: "butler_tableflip",
        stargazers_count: 1,
        url: "https://github.com/keathley/butler_tableflip"
      },
      %AweFunBox.Library{
        description: "A Butler plugin for showing silly photos of Nick Cage.",
        last_commit_date: ~N[2015-12-15 06:05:44],
        name: "butler_cage",
        stargazers_count: 1,
        url: "https://github.com/keathley/butler_cage"
      }
    ],
    topic: "Examples and funny stuff"
  }
]

for collection <- collections, do: AweFunBox.Repo.insert(collection)
